<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Block\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\ListProduct as CategoryListProduct;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Api\AttributeInterface;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\Helper\Data;
use Scandi\Badge\Model\BadgeFactory;
use Scandi\Badge\Model\ResourceModel\Badge as BadgeResourceModel;
use Scandi\Badge\Model\ResourceModel\Badge\CollectionFactory;

/**
 * Class ListProduct
 * @package Scandiweb\Badge\Block\Product
 */
class ListProduct extends CategoryListProduct
{
    /**
     * @var BadgeFactory
     */
    protected $badgeFactory;

    /**
     * @var BadgeResourceModel
     */
    protected $badgeResourceModel;

    /**
     * @var CollectionFactory
     */
    protected $badgeCollectionFactory;

    /**
     * ListProduct constructor.
     * @param BadgeFactory $badgeFactory
     * @param BadgeResourceModel $badgeResourceModel
     * @param CollectionFactory $badgeCollectionFactory
     * @param Context $context
     * @param PostHelper $postDataHelper
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $urlHelper
     * @param array $data
     */
    public function __construct(
        BadgeFactory $badgeFactory,
        BadgeResourceModel $badgeResourceModel,
        CollectionFactory $badgeCollectionFactory,
        Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        array $data = []
    ) {
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
        $this->badgeResourceModel = $badgeResourceModel;
        $this->badgeFactory = $badgeFactory;
        $this->badgeCollectionFactory = $badgeCollectionFactory;
    }

    /**
     * Returns badge data for the specified product
     * by default returns an empty collection if no badge attribute is found
     *
     * @param AttributeInterface $badgeAttribute
     * @return string|null
     */
    public function getBadgeData($badgeAttribute)
    {
        $id = $badgeAttribute->getValue();

        $badgeCollection = $this->badgeCollectionFactory->create();
        $badge = $badgeCollection->getItemById($id);
        $badgeData = $badge->getData();

        /**
         * If status is not on: don't display badge
         * else: display badge
         */
        return !$badgeData['status'] ? null : '<img class="product-badge" src="' . $badgeData['image'] . '" alt="' . $badgeData['title'] . '">';
    }
}
