<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Controller\Adminhtml\Badge;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Scandi\Badge\Model\BadgeFactory;
use Scandi\Badge\Model\ResourceModel\Badge as BadgeResourceModel;

class Delete extends Action
{
    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var BadgeFactory
     */
    protected $badgeFactory;

    /**
     * @var BadgeResourceModel
     */
    protected $badgeResourceModel;

    /**
     * Delete constructor.
     *
     * @param Context $context
     * @param RedirectFactory $resultRedirectFactory
     * @param BadgeFactory $badgeFactory
     * @param BadgeResourceModel $badgeResourceModel
     */
    public function __construct(
        Context $context,
        RedirectFactory $resultRedirectFactory,
        BadgeFactory $badgeFactory,
        BadgeResourceModel $badgeResourceModel
    ) {
        parent::__construct($context);
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->badgeFactory = $badgeFactory;
        $this->badgeResourceModel = $badgeResourceModel;
    }

    /**
     * Deletes the selected Badge from the Product Badges grid
     *
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            $this->messageManager->addErrorMessage(__('Unable to find the Badge to delete.'));
            return $resultRedirect->setPath('*/*/');
        }

        try {
            // Initialize model and delete it :
            $badgeModel = $this->badgeFactory->create();
            $badgeModel->setId($id);
            $this->badgeResourceModel->delete($badgeModel);

            // Display success message :
            $this->messageManager->addSuccessMessage(__('The Badge has been deleted successfully.'));
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__('Error while deleting the badge') . $e->getMessage());
        }

        return $resultRedirect->setPath('*/*/');
    }
}
