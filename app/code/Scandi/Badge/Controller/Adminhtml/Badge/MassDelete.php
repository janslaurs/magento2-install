<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Controller\Adminhtml\Badge;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Component\MassAction\Filter;
use Scandi\Badge\Model\ResourceModel\Badge\CollectionFactory;

/**
 * Class MassDelete
 * POST request receiver class, for deletion of multiple
 * Badge elements
 *
 * @version 1.0.0
 */
class MassDelete extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session (ACL)
     *
     * @var string
     */
    const ADMIN_RESOURCE = 'Scandiweb_Badge::badges_delete';

    /**
     * Controller for badge collection management
     *
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * MassDelete constructor
     *
     * @since 1.0.0
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory)
    {
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Execute action
     * Deletes all Badges that are passed via POST request
     *
     * @since 1.0.0
     * @return Redirect
     * @throws LocalizedException|Exception
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());

        /**
         * Stores total amount of badges, that will be deletes,
         * so that latter we can output message about element deletion
         */
        $collectionSize = $collection->getSize();

        /**
         * Deletes all badges
         */
        foreach ($collection as $badge) {
            $badge->delete();
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 badges have been deleted.', $collectionSize));

        /**
         * Redirects back to table view after deletion
         */
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $redirect->setPath('*/*/');
    }
}
