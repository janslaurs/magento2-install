<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Controller\Adminhtml\Badge;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Cms\Controller\Adminhtml\Page\PostDataProcessor;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Scandi\Badge\Model\Badge;
use Scandi\Badge\Model\BadgeFactory;
use Scandi\Badge\Model\ImageUploader;
use Scandi\Badge\Model\ResourceModel\Badge as BadgeResourceModel;
use Throwable;

/**
 * Class Save
 * @package Scandi\Badge\Controller\Adminhtml\Badge
 */
class Save extends Action
{
    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @var Badge
     */
    protected $model;

    /**
     * @var BadgeResourceModel
     */
    protected $badgeResourceModel;

    /**
     * @var BadgeFactory
     */
    protected $badgeFactory;

    /**
     * Save constructor.
     *
     * @param Context $context
     * @param PostDataProcessor $dataProcessor
     * @param Badge $model
     * @param BadgeResourceModel $badgeResourceModel
     * @param BadgeFactory $badgeFactory
     * @param RedirectFactory $resultRedirectFactory
     * @param DataPersistorInterface $dataPersistor
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context $context,
        PostDataProcessor $dataProcessor,
        Badge $model,
        BadgeResourceModel $badgeResourceModel,
        BadgeFactory $badgeFactory,
        RedirectFactory $resultRedirectFactory,
        DataPersistorInterface $dataPersistor,
        ImageUploader $imageUploader
    ) {
        $this->dataProcessor = $dataProcessor;
        $this->model = $model;
        $this->badgeResourceModel = $badgeResourceModel;
        $this->badgeFactory = $badgeFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->dataPersistor = $dataPersistor;
        $this->imageUploader = $imageUploader;
        parent::__construct($context);
    }

    /**
     * Execute logic on button press
     *
     * @return ResponseInterface|Redirect|ResultInterface
     * @throws Exception
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            $data = $this->dataProcessor->filter($data);
            $data['image'] = $data['image'][0]['url'] ?? null;
            $this->model->setData($data);

            try {
                $this->_eventManager->dispatch(
                    'scandiweb_badge_prepare_save',
                    ['badge' => $this->model, 'request' => $this->getRequest()]
                );
                $this->badgeResourceModel->save($this->model);
                $this->messageManager->addSuccessMessage(__('You have successfully saved the Badge!'));
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (Throwable $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Badge.'));
            }
            $this->dataPersistor->set('scandiweb_badge', $data);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
