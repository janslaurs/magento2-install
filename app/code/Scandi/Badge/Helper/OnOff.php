<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Helper;

/**
 * Source model for element with enable and disable variants.
 * @api
 * @since 100.0.2
 */
class OnOff implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Value which equal On for OnOff dropdown.
     */
    const ENABLE_VALUE = 1;

    /**
     * Value which equal Off for OnOff dropdown.
     */
    const DISABLE_VALUE = 0;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::ENABLE_VALUE, 'label' => __('On')],
            ['value' => self::DISABLE_VALUE, 'label' => __('Off')],
        ];
    }
}
