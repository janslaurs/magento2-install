<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Scandi\Badge\Model\ResourceModel\Badge\CollectionFactory;

/**
 * Class BadgeList
 * Serves as the source of the options available in the badge attribute dropdown in the Product Badge/Edit form
 * @package Scandi\Badge\Model\Attribute\Source
 */
class BadgeList extends AbstractSource
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * BadgeList constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options[] = [
            'value' => '',
            'label' => __('')
        ];
        $badgeCollection = $this->collectionFactory->create();
        $badges = $badgeCollection->getItems();

        foreach ($badges as $badge) {
            $this->_options[] = [
                'value' => $badge->getData('badge_id'),
                'label' => $badge->getData('title')
            ];
        }

        return $this->_options;
    }
}
