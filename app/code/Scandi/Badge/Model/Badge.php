<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Model;

use Magento\Framework\Model\AbstractModel;
use Scandi\Badge\Model\ResourceModel\Badge as ResourceModel;

/**
 * Class Badge
 * @package Scandi\Badge\Model
 */
class Badge extends AbstractModel
{
    /**
     * Initialize the Resource Model
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
