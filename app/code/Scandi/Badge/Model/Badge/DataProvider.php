<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Model\Badge;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Scandi\Badge\Model\ResourceModel\Badge\Collection;
use Scandi\Badge\Model\ResourceModel\Badge\CollectionFactory;

/**
 * Class DataProvider
 * @package Scandi\Badge\Model\Badge
 */
class DataProvider extends AbstractDataProvider
{

    /**
     * Path in the media folder where badges should be stored.
     *
     * @var string
     */
    const BADGE_PATH = 'scandiweb/badge/img/';

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * DataProvider constructor.
     *
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $badgeCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $badgeCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Returns Badge Data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        foreach ($items as $badge) {
            $badgeData = $badge->getData();
            /** $badgeData['image'] = [ '0' => ['name'=>'$name', 'url'=>'$url'] ] because magento */
            $image = $badgeData['image'];
            $badgeData['image'] = [];
            $badgeData['image'][0]['name'] = basename($image);
            $badgeData['image'][0]['url'] = $image;
            $this->loadedData[$badge->getId()] = $badgeData;
        }

        return $this->loadedData;
    }
}
