<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Badge
 * @package Scandi\Badge\Model\ResourceModel
 */
class Badge extends AbstractDb
{
    /**
     * Initialize main table for Badges in DB
     */
    public function _construct()
    {
        $this->_init('scandiweb_badge', 'badge_id');
    }
}
