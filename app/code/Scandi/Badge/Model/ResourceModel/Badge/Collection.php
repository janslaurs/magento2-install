<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Model\ResourceModel\Badge;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Scandi\Badge\Model\Badge;
use Scandi\Badge\Model\ResourceModel\Badge as ResourceModel;

/**
 * Class Collection
 * Collection controller for badges
 * (Multiple badge, database interaction class)
 *
 * @version 1.0.0
 */
class Collection extends AbstractCollection
{
    /**
     * Tables identification filed name
     *
     * @var string
     */
    protected $_idFieldName = 'badge_id';

    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'scandi_badge_collection';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'badge_collection';

    /**
     * Collection constructor
     *
     * @since 1.0.0
     */
    protected function _construct()
    {
        $this->_init(Badge::class, ResourceModel::class);
    }
}
