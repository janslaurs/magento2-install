<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Scandi\Badge\Model\Attribute\Source\BadgeList;
use Zend_Validate_Exception;

/**
 * Class AddBadgeAttribute
 * @package Scandi\Badge\Setup\Patch\Data
 */
class AddBadgeAttribute implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * AddBadgeAttribute constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Adds the attribute to the General Attributes section in the Product Edit and Add Form
     *
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        /** Add Badges attribute to Product form : */
        $eavSetup->addAttribute(
            Product::ENTITY,
            'badge_attribute',
            [
                'group' => 'General',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Badge',
                'input' => 'select',
                'class' => '',
                'source' => BadgeList::class,
                'global' => 0,
                'sort_order' => 99,
                'used_in_product_listing' => true,
                'default' => '',
                'user_defined' => false,
                'visible' => true,
                'required' => false,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'unique' => false,
                'apply_to' => ''
            ]
        );

        $attributeId = $eavSetup->getAttributeId('catalog_product', 'badge_attribute');
        $options = [
            'values' => [],
            'attribute_id' => $attributeId
        ];

        $eavSetup->addAttributeOption($options);
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }
}
