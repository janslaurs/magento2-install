<?php
/**
 * @category     Scandi
 * @package      Scandi_Badge
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Badge\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Actions
 * @package Scandi\Badge\Ui\Component\Listing\Column
 */
class Actions extends Column
{
    /**
     * @var string
     */
    const EDIT_URL_PATH = 'promotions/badge/edit';

    /**
     * @var string
     */
    const DELETE_URL_PATH = 'promotions/badge/delete';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Actions constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array|void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = [
                    'edit' => [
                        'href' => $this->urlBuilder->getUrl(
                            self::EDIT_URL_PATH,
                            [
                                'id' => $item['badge_id']
                            ]
                        ),
                        'label' => __('Edit'),
                        'hidden' => false
                    ],
                    'delete' => [
                        'href' => $this->urlBuilder->getUrl(
                            self::DELETE_URL_PATH,
                            [
                                'id' => $item['badge_id']
                            ]
                        ),
                        'label' => __('Delete'),
                        'hidden' => false,
                        'confirm' => [
                            'title' => __('Delete ${ $.$data.title }'),
                            'message' => __('Are you sure you want to delete the badge \'${ $.$data.title }\'?')
                        ]
                    ]
                ];
            }
        }

        return $dataSource;
    }
}
