<?php
/**
 * @category     Scandi
 * @package      Scandi_Module
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Module\Setup\Patch\Data;

use Magento\Directory\Model\ResourceModel\Currency;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

class SetCurrencyRates implements DataPatchInterface, PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param Currency $currency
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        Currency $currency
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->currency = $currency;
    }

    /**
     * {@inheritdoc}
     * @throws LocalizedException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->currency->saveRates([
            'EUR' => [ 'GBP' => 0.89, 'USD' => 1.1  ],
            'GBP' => [ 'EUR' => 1.10, 'USD' => 1.23 ],
            'USD' => [ 'GBP' => 0.82, 'EUR' => 0.90 ]
        ]);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            \Scandi\Module\Setup\Patch\Data\SetupCurrency::class
        ];
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    public static function getVersion()
    {
        return '1.0.1';
    }
}
