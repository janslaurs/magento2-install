<?php
/**
 * @category     Scandi
 * @package      Scandi_Module
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Module\Setup\Patch\Data;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\StoreFactory;

class SetupNewStores implements DataPatchInterface, PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var StoreFactory
     */
    protected $storeFactory;

    /**
     * @var Store
     */
    protected $storeResourceModel;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param StoreFactory $storeFactory
     * @param Store $storeResourceModel
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        StoreFactory $storeFactory,
        Store $storeResourceModel
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->storeFactory = $storeFactory;
        $this->storeResourceModel = $storeResourceModel;
    }

    /**
     * {@inheritdoc}
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $stores =
            [
                [
                    'code' => 'britain_store',
                    'name' => 'Britain Store',
                    'currency' => 'GBP',
                ],
                [
                    'code' => 'german_store',
                    'name' => 'German Store',
                    'currency' => 'EUR',
                ],
            ];

        foreach ($stores as $_store) {
            $store = $this->storeFactory->create();
            $store->setCode($_store['code']);
            $store->setName($_store['name']);
            $store->setGroupId(1);
            $store->setStoreGroupId(1);
            $store->setWebsiteId(1);
            $store->setCurrentCurrencyCode($_store['currency']);
            $store->setData('is_active', '1');
            $this->storeResourceModel->save($store);
        }
        $this->moduleDataSetup->endSetup();

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    public static function getVersion()
    {
        return '1.0.1';
    }
}
