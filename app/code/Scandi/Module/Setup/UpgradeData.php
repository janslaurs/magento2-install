<?php
/**
 * @category     Scandi
 * @package      Scandi_Module
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandi\Module\Setup;

use Magento\Cms\Model\PageFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * Construct
     *
     * @param PageFactory $pageFactory
     */
    public function __construct(
        PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0) {
            $page = $this->_pageFactory->create();
            $page->setTitle('Example CMS page')
                ->setContentHeading('example heading')
                ->setIdentifier('example-cms')
                ->setIsActive(true)
                ->setPageLayout('1column')
                ->setData('stores', [0])
                ->setContent('{{block class="Magento\\Framework\\View\\Element\\Template" block_id="delivery" template="Scandi_Module::cms_content.phtml"}}')
                ->save();
        }

        $setup->endSetup();
    }
}
