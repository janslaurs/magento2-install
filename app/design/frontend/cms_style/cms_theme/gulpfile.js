/**
 * @category     cms_style
 * @package      cms_style/cms_theme
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
let gulp = require('gulp');
let sass = require('gulp-sass');

function style() {
    return (
        gulp.src('web/scss/main.scss')
            .pipe(sass())
            .on("error", sass.logError)
            .pipe(gulp.dest('web/css'))
    );
}

function watch(){
    gulp.watch('styles/*.sass', style)
}

exports.style = style
exports.watch = watch
