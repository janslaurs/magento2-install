/**
 * @category     cms_style
 * @package      cms_style/cms_theme
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
let config = {
    paths: {
        slick: 'js/slick'
    },
    map: {
        '*': {
            carousel: 'js/carousel'
        }
    },
    deps: ['js/header'],
    shim: {
        slick: {
            'js/slick': ['jquery'],
            'js/carousel': ['jquery'],
        }
    }
};
