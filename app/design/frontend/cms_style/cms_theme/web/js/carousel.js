/**
 * @category     cms_style
 * @package      cms_style/cms_theme
 * @author       Janis Laurins info@scandiweb.com
<<<<<<< HEAD
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
=======
 * @copyright    Copyright (c) 2020 Scandiweb, Inc(https://scandiweb.com)
>>>>>>> 359ad6e9a9ce7ab378df048dc0cd21b0ce573ca9
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
require([
    'jquery',
    'slick'
], function ($) {
    let slider = $('.responsive');
    $(function () {
        slider.slick({
            mobileFirst: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 3600,
            dots: true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: true,
                        centerPadding: '10px',

                    }
                }
            ]
        });
    });
});
