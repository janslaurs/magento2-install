/**
 * @category     cms_style
 * @package      cms_style/cms_theme
 * @author       Janis Laurins info@scandiweb.com
 * @copyright    Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

require([
    'jquery'
], function ($) {

    const TOPDISTANCE = 400;
    let searchPos = $('.block-search').offset();
    let addToCartPos = $('.minicart-wrapper').offset();

    $('.page-header').addClass('sticky');
    $('.sections').addClass('sticky-bottom');
    $('.logo').addClass('sticky-logo');
    $('.block-search').addClass('sticky-search').css({'left': searchPos.left});
    $('.minicart-wrapper').addClass('sticky-minicart-wrapper').css({'left': addToCartPos.left});

    $(window).scroll(function () {
        if ($('.page-header').offset().top >= TOPDISTANCE) {
            $('.logo').removeClass('logo-transition-down').addClass('logo-transition-up');
            $('.block-search, .minicart-wrapper').removeClass('block-search-transition-down').addClass('block-search-transition-up');
            $('.page-header').addClass('page-header-transition');
            $('.sections').addClass('sections-transition');
        } else {
            $('.logo').removeClass('logo-transition-up').addClass('logo-transition-down');
            $('.block-search, .minicart-wrapper').removeClass('block-search-transition-up').addClass('block-search-transition-down');
            $('.page-header').removeClass('page-header-transition');
            $('.sections').removeClass('sections-transition');
        }
    });
});
